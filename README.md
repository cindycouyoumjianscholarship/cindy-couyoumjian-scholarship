Cindy Couyoumjian is a Certified Financial Advisor with Cinergy Financial. In this role, Cindy brings dynamic investment strategies to her clients, customizing each portfolio to address the transitions in ones life, all while staying abreast on the ever-changing economic landscape.

In 2019, Cindy announced the creation of an annual scholarship campaign to help post-secondary students in North America. She hopes her contribution will encourage them to never stop learning and to develop a lifelong career they are passionate about.

Website : http://www.cindycouyoumjianscholarship.com/